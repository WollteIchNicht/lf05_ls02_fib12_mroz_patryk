import java.util.*;

public class Raumschif {

	private int torpedeoanzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
    private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator= new ArrayList<String>();
	private ArrayList<Ladung>ladungsverzeichnis= new ArrayList<Ladung>();
	

	public Raumschif() {}
		

	public Raumschif(int torpedeoanzahl, 
					int energieversorgungInProzent, 
					int schildeInProzent, 
					int huelleInProzent,
					int lebenserhaltungssystemeInProzent, 
					int androidenAnzahl, 
					String schiffsname){
		
		this.torpedeoanzahl = torpedeoanzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		
	}

	public int getTorpedeoanzahl() {
		return torpedeoanzahl;
	}

	public void setTorpedeoanzahl(int torpedeoanzahl) {
		this.torpedeoanzahl = torpedeoanzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	public void addLadung(Ladung neueLadung) {
        this.ladungsverzeichnis.add(neueLadung);
    }
	
	public String toString() {
		return "Raumschiff: \n"
				+ "   torpedeoanzahl: " + this.torpedeoanzahl +"\n"
				+ "   energieversorgungInProzent: " + this.energieversorgungInProzent +"\n"
				+ "   schildeInProzent: " + this.schildeInProzent +"\n"
				+ "   huelleInProzent: " + this.huelleInProzent +"\n"
				+ "   lebenserhaltungssystemeInProzent: " + this.lebenserhaltungssystemeInProzent +"\n"
				+ "   schiffsname: " + this.schiffsname +"\n"
				+ "   androidenAnzahl: " + this.androidenAnzahl +"\n";
	}
}

