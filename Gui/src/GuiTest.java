import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

public class GuiTest {

	private JFrame frame;
	private JTextField textUserEingabe;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GuiTest window = new GuiTest();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GuiTest() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.BLACK);
		frame.getContentPane().setForeground(Color.BLACK);
		frame.setForeground(Color.PINK);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblTitel = new JLabel("Doun`t press me");
		lblTitel.setBackground(Color.YELLOW);
		lblTitel.setForeground(Color.RED);
		lblTitel.setFont(new Font("Tw Cen MT", Font.BOLD | Font.ITALIC, 18));
		frame.getContentPane().add(lblTitel, BorderLayout.CENTER);
		
		JButton btnButton = new JButton("Press Me");
		btnButton.setFont(new Font("Source Sans Pro Black", Font.BOLD | Font.ITALIC, 14));
		btnButton.setForeground(Color.GREEN);
		btnButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("Hallo World");
				frame.getContentPane().setBackground(Color.PINK);
				
			}
		});
		frame.getContentPane().add(btnButton, BorderLayout.SOUTH);
		
		JButton btnButtonText = new JButton("Ich zeige dir vieles ");
		btnButtonText.setForeground(Color.GREEN);
		btnButtonText.setFont(new Font("Source Sans Pro Semibold", Font.ITALIC, 14));
		btnButtonText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String eingabeText = textUserEingabe.getText();
				System.out.println(eingabeText);
				frame.getContentPane().setBackground(Color.GREEN);
			}
		});
		frame.getContentPane().add(btnButtonText, BorderLayout.NORTH);
		
		textUserEingabe = new JTextField();
		textUserEingabe.setForeground(Color.WHITE);
		textUserEingabe.setText("Momoin");
		frame.getContentPane().add(textUserEingabe, BorderLayout.WEST);
		textUserEingabe.setColumns(10);
	}

}
